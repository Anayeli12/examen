package facci.pm.cedeno_anayely.examenfinal.Activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import facci.pm.cedeno_anayely.examenfinal.Adapter.AdaptadorProfesores;
import facci.pm.cedeno_anayely.examenfinal.Modelo.Profesores;
import facci.pm.cedeno_anayely.examenfinal.R;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private static final String URL = "http://157.230.149.120:3003/profesores";
    private ArrayList<Profesores> profesoresArrayList;
    private RecyclerView.LayoutManager layoutManager;
    private AdaptadorProfesores adaptadorProfesores;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView)findViewById(R.id.Recycler);
        profesoresArrayList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adaptadorProfesores = new AdaptadorProfesores(profesoresArrayList);
        progressDialog = new ProgressDialog(this);

        ListaProfesores();
    }

    private void ListaProfesores() {
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i<jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Profesores profesores = new Profesores();
                        profesores.setApellido(jsonObject.getString("apellido"));
                        profesores.setNombre(jsonObject.getString("nombre"));
                        profesores.setCedula(jsonObject.getString("cedula"));
                        profesores.setImagen(jsonObject.getString("imagen"));
                        profesores.setMateria(jsonObject.getString("materia"));
                        profesores.setTitular(String.valueOf(jsonObject.getBoolean("titular")));
                        profesores.setId(String.valueOf(jsonObject.getInt("id")));
                        profesoresArrayList.add(profesores);
                    }
                    recyclerView.setAdapter(adaptadorProfesores);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
