package facci.pm.cedeno_anayely.examenfinal.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import facci.pm.cedeno_anayely.examenfinal.R;

public class Main2Activity extends AppCompatActivity {

    private static final String URL_DETALLE = "http://157.230.149.120:3003/profesor/";
    private TextView nombre, apellido;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        String id = getIntent().getStringExtra("id");

        nombre = (TextView)findViewById(R.id.NombreProffD);
        apellido = (TextView)findViewById(R.id.ApellidoProffD);
        imageView = (ImageView)findViewById(R.id.IMGProffD);

        DetalleProfesor(id);
    }

    private void DetalleProfesor(String id) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_DETALLE + id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    nombre.setText(jsonObject.getString("nombre"));
                    apellido.setText(jsonObject.getString("apellido"));
                    Picasso.get().load(jsonObject.getString("imagen")).into(imageView);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
